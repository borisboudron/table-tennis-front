import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/be';

registerLocaleData(localeFr, 'fr-BE', localeFrExtra);

import {AppComponent} from './app.component';
import {HomeComponent} from './components/home/home.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {YoutubePlayerModule} from 'ngx-youtube-player';
import {RoundPipe} from './components/customPipes/round.pipe';
import {ListingComponent} from './components/listing/listing.component';
import {SortPipe} from './components/customPipes/sort.pipe';
import {PlayerComponent} from './components/player/player.component';
import {AbsencesComponent} from './components/absences/absences.component';
import {AbsenceComponent} from './components/absences/absence/absence.component';
import {NewAbsenceComponent} from './components/absences/new-absence/new-absence.component';
import {SelectionsComponent} from './components/selections/selections.component';
import {TeamSelectionComponent} from './components/selections/team-selection/team-selection.component';
import {PlayerSelectorComponent} from './components/selections/team-selection/player-selector/player-selector.component';
import {FeuilleMatchComponent} from './components/feuille-match/feuille-match.component';
import { CompetitionComponent } from './components/competition/competition.component';
import { PlayerSerieAdjusterComponent } from './components/competition/player-serie-adjuster/player-serie-adjuster.component';
import { SettingsComponent } from './components/competition/settings/settings.component';
import { ClubComponent } from './components/club/club.component';
import { TeamComponent } from './components/club/team/team.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    RoundPipe,
    ListingComponent,
    SortPipe,
    PlayerComponent,
    AbsencesComponent,
    AbsenceComponent,
    NewAbsenceComponent,
    SelectionsComponent,
    TeamSelectionComponent,
    PlayerSelectorComponent,
    FeuilleMatchComponent,
    CompetitionComponent,
    PlayerSerieAdjusterComponent,
    SettingsComponent,
    ClubComponent,
    TeamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    YoutubePlayerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{provide: LOCALE_ID, useValue: 'fr-BE'}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
