import {Club} from './club';

export class Team {
  constructor(
    private _id: number,
    private _clubId: string,
    private _club: Club,
    private _letter: string
  ) { }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get clubId(): string {
    return this._clubId;
  }

  set clubId(value: string) {
    this._clubId = value;
  }

  get club(): Club {
    return this._club;
  }

  set club(value: Club) {
    this._club = value;
  }

  get letter(): string {
    return this._letter;
  }

  set letter(value: string) {
    this._letter = value;
  }
}
