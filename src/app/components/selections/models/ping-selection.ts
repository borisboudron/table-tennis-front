import {Team} from './team';
import {Player} from '../../player/models/player';

export class PingSelection {
  private _players: Player[] = [];
  constructor (
    private _id: number,
    private _weekDay: number,
    private _teamId: number,
    private _team: Team,
    private _homeOrAway: string,
    private _player1Id: number,
    private _player1: Player,
    private _player2Id: number,
    private _player2: Player,
    private _player3Id: number,
    private _player3: Player,
    private _player4Id: number,
    private _player4: Player
  ) {
    this._players.push(this.player1);
    this._players.push(this.player2);
    this._players.push(this.player3);
    this._players.push(this.player4);
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get weekDay(): number {
    return this._weekDay;
  }

  set weekDay(value: number) {
    this._weekDay = value;
  }

  get teamId(): number {
    return this._teamId;
  }

  set teamId(value: number) {
    this._teamId = value;
  }

  get team(): Team {
    return this._team;
  }

  set team(value: Team) {
    this._team = value;
  }

  get homeOrAway(): string {
    return this._homeOrAway;
  }

  set homeOrAway(value: string) {
    this._homeOrAway = value;
  }

  get player1Id(): number {
    return this._player1Id;
  }

  set player1Id(value: number) {
    this._player1Id = value;
  }

  get player1(): Player {
    return this._player1;
  }

  set player1(value: Player) {
    this._player1 = value;
  }

  get player2Id(): number {
    return this._player2Id;
  }

  set player2Id(value: number) {
    this._player2Id = value;
  }

  get player2(): Player {
    return this._player2;
  }

  set player2(value: Player) {
    this._player2 = value;
  }

  get player3Id(): number {
    return this._player3Id;
  }

  set player3Id(value: number) {
    this._player3Id = value;
  }

  get player3(): Player {
    return this._player3;
  }

  set player3(value: Player) {
    this._player3 = value;
  }

  get player4Id(): number {
    return this._player4Id;
  }

  set player4Id(value: number) {
    this._player4Id = value;
  }

  get player4(): Player {
    return this._player4;
  }

  set player4(value: Player) {
    this._player4 = value;
  }

  get players(): Player[] {
    return this._players;
  }

  set players(value: Player[]) {
    this._players = value;
  }
}
