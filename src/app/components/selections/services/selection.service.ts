import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SelectionService {

  constructor(private client: HttpClient) {
  }

  add(weekDay: number, selections: any[]): Observable<number> {
    let httpParams = new HttpParams();
    for (const property of Object.keys(selections)) {
      httpParams = httpParams.append(property, selections[property]);
    }
    return this.client.post<number>('http://tttheux.local/selections/' + weekDay, httpParams);
  }
}
