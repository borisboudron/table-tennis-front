import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Team} from '../models/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private client: HttpClient) { }
  getAllByClub(id: string): Observable<Team[]> {
    return this.client.get<Team[]>('http://tttheux.local/clubs/' + id + '/teams');
  }
  getAllByClub2(id: string): Observable<any> {
    return this.client.get<any>('http://tttheux.local/clubs/' + id + '/teams2');
  }
}
