import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../player/models/player';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-player-selector',
  templateUrl: './player-selector.component.html',
  styleUrls: ['./player-selector.component.css']
})
export class PlayerSelectorComponent implements OnInit {
  private _players: Player[];
  private _possiblePlayers: Player[] = null;
  private _firstPosition: number;
  private _control: FormControl;

  get players(): Player[] {
    return this._players;
  }

  @Input() set players(value: Player[]) {
    this._players = value;
  }

  get possiblePlayers(): Player[] {
    return this._possiblePlayers;
  }

  set possiblePlayers(value: Player[]) {
    this._possiblePlayers = value;
  }

  get firstPosition(): number {
    return this._firstPosition;
  }

  @Input() set firstPosition(value: number) {
    this._firstPosition = value;
  }

  get control(): FormControl {
    return this._control;
  }

  @Input() set control(value: FormControl) {
    this._control = value;
  }

  constructor() { }

  ngOnInit() {
    this.possiblePlayers = [];
    for (const player of this.players) {
      if (player.index >= this.firstPosition) {
        this.possiblePlayers.push(player);
      }
    }
  }

}
