import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../player/models/player';
import {Team} from '../models/team';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-team-selection',
  templateUrl: './team-selection.component.html',
  styleUrls: ['./team-selection.component.css']
})
export class TeamSelectionComponent implements OnInit {
  get players(): Player[] {
    return this._players;
  }

  @Input() set players(value: Player[]) {
    this._players = value;
  }

  get team(): Team {
    return this._team;
  }

  @Input() set team(value: Team) {
    this._team = value;
  }

  get teamNumber(): number {
    return this._teamNumber;
  }

  set teamNumber(value: number) {
    this._teamNumber = value;
  }

  get selections(): FormGroup {
    return this._selections;
  }

  @Input() set selections(value: FormGroup) {
    this._selections = value;
  }

  constructor() {
  }

  private _players: Player[];
  private _team: Team = null;
  private _teamNumber: number;
  private _selections: FormGroup = null;

  static toNumber(letter: string) {
    return letter.charCodeAt(0) - 64;
  }

  ngOnInit() {
    this._teamNumber = 4 * (TeamSelectionComponent.toNumber(this.team.letter) - 1);
  }

}
