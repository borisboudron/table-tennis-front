import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../listing/services/player.service';
import {TeamService} from './services/team.service';
import {SelectionService} from './services/selection.service';
import {FormArray, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Player} from '../player/models/player';

@Component({
  selector: 'app-selections',
  templateUrl: './selections.component.html',
  styleUrls: ['./selections.component.css']
})
export class SelectionsComponent implements OnInit {
  get weekDay(): number {
    return this._weekDay;
  }

  set weekDay(value: number) {
    this._weekDay = value;
  }

  get days(): number[] {
    return this._days;
  }

  set days(value: number[]) {
    this._days = value;
  }

  get players(): any[] {
    return this._players;
  }

  set players(value: any[]) {
    this._players = value;
  }

  get teams(): any[] {
    return this._teams;
  }

  set teams(value: any[]) {
    this._teams = value;
  }

  get selections(): FormArray {
    return this._selections;
  }

  set selections(value: FormArray) {
    this._selections = value;
  }

  constructor(
    private playerService: PlayerService,
    private teamService: TeamService,
    private selectionService: SelectionService,
    private builder: FormBuilder
  ) {
  }

  private _weekDay = 1;
  private _days: number[];
  private _players: any[] = null;
  private _teams: any[] = null;
  private _selections: FormArray;

  ngOnInit() {
    this.days = [];
    for (let i = 1; i <= 22; i++) {
      this._days.push(i);
    }
    this.playerService.getAll().subscribe(players => {
      this.players = players;
      this.teamService.getAllByClub('L184').subscribe(teams => {
        const teamCount = teams.length;
        teams.sort((t1, t2) => {
          if (t1.letter === t2.letter) {
            return 0;
          }
          return (t1.letter < t2.letter) ? -1 : 1;
        });
        this.teams = teams;

        this.selections = this.builder.array([]);
        for (let t = 0; t < teamCount; t++) {
          const config = {};
          config['team' + (t + 1) + 'Id'] = [this.teams[t].id, [
            Validators.required
          ]];
          config['player' + (4 * t + 1) + 'Id'] = ['', [
            Validators.required
          ]];
          config['player' + (4 * t + 2) + 'Id'] = ['', [
            Validators.required
          ]];
          config['player' + (4 * t + 3) + 'Id'] = ['', [
            Validators.required
          ]];
          config['player' + (4 * t + 4) + 'Id'] = ['', [
            Validators.required
          ]];
          this.selections.push(this.builder.group(config, {
            validator: [
              SelectionDifferentPlayersValidator(1, t),
              SelectionOrderedPlayers(t, this.players),
            ],
          }));
        }
        this.selections.setValidators([
            SelectionDifferentPlayersValidator(teamCount),
            ThirdUpBetterThanFirstDownValidator(teamCount, this.players),
          ]
        );
      });
    });
  }

  dayChanged() {
    // this.loadDayAbsences(this.weekDay);
  }

  saveAll() {
    // Only post valid selections
    const validSelections = [];
    for (const selection of this.selections.controls) {
      if (selection.valid) {
        validSelections.push(selection.value);
      }
    }
    console.log(this.selections);
    console.log(validSelections);
    // this.selectionService.add(this.weekDay, validSelections).subscribe(id => {
    // });
  }
}

export function NotNumberValidator(): ValidatorFn {
  return (control: FormGroup): { [key: string]: any } | null => {
    const regEx: RegExp = /[\d]+/i;
    const check = regEx.test(control.value);
    return check ? {
      notNumber: {
        value: control.value
      }
    } : null;
  };
}

export function SelectionDifferentPlayersValidator(teamCount: number, teamNumber = 0): ValidatorFn {
  return (control: FormArray | FormGroup): ValidationErrors | null => {
    const ckPlayers = {};
    for (let t = teamNumber; t < teamCount + teamNumber; t++) {
      for (let p = 4 * t + 1; p <= 4 * (t + 1); p++) {
        if (teamCount > 1) {
          ckPlayers[p] = control.get([t]).get('player' + p + 'Id').value;
        } else {
          ckPlayers[p] = control.get('player' + p + 'Id').value;
        }
      }
    }
    const errors = {};
    for (let p1 = teamNumber + 1; p1 < 4 * (teamCount + teamNumber); p1++) {
      for (let p2 = p1 + 1; p2 <= 4 * (teamCount + teamNumber); p2++) {
        if (ckPlayers[p1] && ckPlayers[p2] && ckPlayers[p1] === ckPlayers[p2]) {
          errors['Joueurs ' + p1 + ' et ' + p2] = true;
        }
      }
    }
    return (errors !== {}) ? errors : null;
  };
}

export function ThirdUpBetterThanFirstDownValidator(teamCount: number, players: Player[]): ValidatorFn {
  return (control: FormArray): ValidationErrors | null => {
    const errors = {};
    for (let t = 0; t < teamCount - 1; t++) {
      const up = control.get([t]).get('player' + (4 * t + 3) + 'Id').value;
      const down = control.get([(t + 1)]).get('player' + (4 * (t + 1) + 1) + 'Id').value;
      const playerUp = findPlayer(up, players);
      const playerDown = findPlayer(down, players);
      if (playerUp && playerDown && playerUp.ranking > playerDown.ranking) {
        errors['Équipes ' + t + ' et ' + (t + 1)] = true;
      }
    }
    return (errors !== {}) ? errors : null;
  };
}

export function SelectionOrderedPlayers(teamNumber: number, players: Player[]): ValidatorFn {
  return (control: FormGroup): ValidationErrors | null => {
    const errors = {};
    for (let p = 4 * teamNumber + 1; p < 4 * (teamNumber + 1) - 1; p++) {
      const before = control.get('player' + p + 'Id').value;
      const after = control.get('player' + (p + 1) + 'Id').value;
      const playerBefore = findPlayer(before, players);
      const playerAfter = findPlayer(after, players);
      if (playerBefore && playerAfter && playerBefore.ranking > playerAfter.ranking) {
        errors['Équipe ' + teamNumber + ' : joueurs ' + p + ' et ' + (p + 1)] = true;
      }
    }
    return (errors !== {}) ? errors : null;
  };
}

export function findPlayer(playerId: number, players: Player[]) {
  for (let p = 0; p < players.length; p++) {
    // tslint:disable-next-line:triple-equals
    if (players[p].id == playerId) {
      return players[p];
    }
  }
  return null;
}
