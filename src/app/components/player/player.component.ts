import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Player} from './models/player';
import {PlayerService} from '../listing/services/player.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  private _id: number;
  private _player: Player;
  private _form: FormGroup;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get player(): Player {
    return this._player;
  }

  set player(value: Player) {
    this._player = value;
  }

  get form(): FormGroup {
    return this._form;
  }

  set form(value: FormGroup) {
    this._form = value;
  }

  constructor(private router: Router, private route: ActivatedRoute, private playerService: PlayerService, private builder: FormBuilder) {
    this.form = this.builder.group({
      'id': [{value: this.id, disabled: true}, [
        Validators.required
      ]],
      'firstName': [{value: '', disabled: true}, [
        Validators.required
      ]],
      'lastName': [{value: '', disabled: true}, [
        Validators.required
      ]],
      'ranking': [{value: '', disabled: true}],
      'training': [{value: ''}],
      'teams': [{value: ''}],
      'fridayEvening': [{value: ''}],
      'saturdayAfternoon': [{value: ''}],
      'saturdayEvening': [{value: ''}],
      'driver': [{value: ''}],
      'backup': [{value: ''}],
    }, {
      validator: []
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.playerService.getByID(this.id).subscribe(player => {
        this.player = player;
      });
    });
  }

  updatePlayer() {
    let toUpdate = null;
    // Check for changed params
    for (const param of Object.keys(this.form.value)) {
      if (this.form.controls[param].dirty) {
        // tslint:disable-next-line:triple-equals
        if (this.player[param] != this.form.controls[param].value) {
          if (toUpdate === null) {
            toUpdate = {};
          }
          toUpdate[param] = this.form.controls[param].value;
        }
      }
    }
    if (toUpdate !== null) {
      this.playerService.update(this.id, toUpdate).subscribe(updated => {
        console.log(updated);
        // Update changed params
        for (const param of Object.keys(toUpdate)) {
          this.player[param] = toUpdate[param];
        }
      });
    }
  }
}
