export class Player {
  private _victories = 0;
  constructor(
    private _id: number,
    private _index: number,
    private _lastName: string,
    private _firstName: string,
    private _ranking: string = 'NC',
    private _training: number | false = null,
    private _teams: string = null,
    private _fridayEvening: number | false = null,
    private _saturdayAfternoon: number | false = null,
    private _saturdayEvening: number | false = null,
    private _driver: string | false = null,
    private _backup: boolean = null,
    private _remarks: string = null,
    private _position: number
  ) { }

  get id(): number {
    return this._index;
  }

  set id(value: number) {
    this._index = value;
  }

  get index(): number {
    return this._index;
  }

  set index(value: number) {
    this._index = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get ranking(): string {
    return this._ranking;
  }

  set ranking(value: string) {
    this._ranking = value;
  }

  get training(): number | false {
    return this._training;
  }

  set training(value: number | false) {
    this._training = value;
  }

  get teams(): string {
    return this._teams;
  }

  set teams(value: string) {
    this._teams = value;
  }

  get fridayEvening(): number | false {
    return this._fridayEvening;
  }

  set fridayEvening(value: number | false) {
    this._fridayEvening = value;
  }

  get saturdayAfternoon(): number | false {
    return this._saturdayAfternoon;
  }

  set saturdayAfternoon(value: number | false) {
    this._saturdayAfternoon = value;
  }

  get saturdayEvening(): number | false {
    return this._saturdayEvening;
  }

  set saturdayEvening(value: number | false) {
    this._saturdayEvening = value;
  }

  get driver(): string | false {
    return this._driver;
  }

  set driver(value: string | false) {
    this._driver = value;
  }

  get backup(): boolean {
    return this._backup;
  }

  set backup(value: boolean) {
    this._backup = value;
  }

  get remarks(): string {
    return this._remarks;
  }

  set remarks(value: string) {
    this._remarks = value;
  }

  get victories(): number {
    return this._victories;
  }

  set victories(value: number) {
    this._victories = value;
  }

  get position(): number {
    return this._position;
  }

  set position(value: number) {
    this._position = value;
  }

  public static getSerie(ranking: string) {
    switch (ranking) {
      case 'B0':
      case 'B2':
      case 'B4':
        return 'DIVISION 1';
      case 'B6':
      case 'C0':
      case 'C2':
        return 'DIVISION 2';
      case 'C4':
      case 'C6':
      case 'D0':
        return 'DIVISION 3';
      case 'D2':
      case 'D4':
      case 'D6':
        return 'DIVISION 4';
      case 'E0':
      case 'E2':
      case 'E4':
        return 'DIVISION 5';
      case 'E6':
      case 'NC':
        return 'DIVISION 6';
      default:
        return '';
    }
  }
}

export enum Ranking {
  NC = 0,
  E6,
  E4,
  E2,
  E0,
  D6,
  D4,
  D2,
  D0,
  C6,
  C4,
  C2,
  C0,
  B6,
  B4,
  B2,
  B0,
  A
}
