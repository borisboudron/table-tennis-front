import {Pipe, PipeTransform} from '@angular/core';
import {Player} from '../player/models/player';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: Player[], method: any): any {
    if (method === 'name') {
      return value.sort((p1, p2) => {
        if (p1.lastName === p2.lastName) {
          if (p1.firstName === p2.firstName) {
            return 0;
          } else {
            return (p1.firstName > p2.firstName) ? 1 : -1;
          }
        } else {
          return (p1.lastName > p2.lastName) ? 1 : -1;
        }
      });
    } else if (method === 'ranking') {
      return value.sort((p1, p2) => {
        if (p1.ranking === p2.ranking) {
          if (p1.lastName === p2.lastName) {
            if (p1.firstName === p2.firstName) {
              return 0;
            } else {
              return (p1.firstName > p2.firstName) ? 1 : -1;
            }
          } else {
            return (p1.lastName > p2.lastName) ? 1 : -1;
          }
        } else {
          return (p1.ranking > p2.ranking) ? 1 : -1;
        }
      });
    }
  }
}
