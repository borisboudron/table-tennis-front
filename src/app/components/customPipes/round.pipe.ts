import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'round'
})
export class RoundPipe implements PipeTransform {

  transform(value: any, mode: any): any {
    if (mode === 'ceil') {
      return Math.ceil(value);
    } else if (mode === 'floor') {
      return Math.floor(value);
    } else {
      return Math.round(value);
    }
  }
}
