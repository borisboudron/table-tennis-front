import {Component, OnInit} from '@angular/core';
import {Interclubs} from './models/interclubs';
import {ActivatedRoute, Router} from '@angular/router';
import {PingSet} from './models/ping-set';
import {Match} from './models/match';
import {InterclubsService} from './services/interclubs.service';
import {PingSetService} from './services/ping-set.service';
import {MatchService} from './services/match.service';

@Component({
  selector: 'app-feuille-match',
  templateUrl: './feuille-match.component.html',
  styleUrls: ['./feuille-match.component.css']
})
export class FeuilleMatchComponent implements OnInit {
  private _r: number;
  private _id: number;
  private _interclubs: Interclubs;
  private _positions = [
    [
      '1-A',
      '2-B',
      '3-C',
      '4'
    ], [
      '1-X',
      '2-Y',
      '3-Z',
      '4'
    ]
  ];
  private _matchesArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
  private _setsArray = [0, 1, 2, 3, 4, 5, 6];
  private _homeScoreEvolution: number[] = [];
  private _awayScoreEvolution: number[] = [];
  private _homeTotalSets = 0;
  private _awayTotalSets = 0;

  get r(): number {
    return this._r;
  }

  set r(value: number) {
    this._r = value;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get interclubs(): Interclubs {
    return this._interclubs;
  }

  set interclubs(value: Interclubs) {
    this._interclubs = value;
  }

  get positions(): string[][] {
    return this._positions;
  }

  set positions(value: string[][]) {
    this._positions = value;
  }

  get matchesArray(): number[] {
    return this._matchesArray;
  }

  set matchesArray(value: number[]) {
    this._matchesArray = value;
  }

  get setsArray(): number[] {
    return this._setsArray;
  }

  set setsArray(value: number[]) {
    this._setsArray = value;
  }

  get homeScoreEvolution(): number[] {
    return this._homeScoreEvolution;
  }

  set homeScoreEvolution(value: number[]) {
    this._homeScoreEvolution = value;
  }

  get awayScoreEvolution(): number[] {
    return this._awayScoreEvolution;
  }

  set awayScoreEvolution(value: number[]) {
    this._awayScoreEvolution = value;
  }

  get homeTotalSets(): number {
    return this._homeTotalSets;
  }

  set homeTotalSets(value: number) {
    this._homeTotalSets = value;
  }

  get awayTotalSets(): number {
    return this._awayTotalSets;
  }

  set awayTotalSets(value: number) {
    this._awayTotalSets = value;
  }

  constructor(private router: Router,
              private route: ActivatedRoute,
              private interclubsService: InterclubsService,
              private setService: PingSetService,
              private matchService: MatchService) {
  }

  ngOnInit() {
    this.r = Math.random();
    this.route.params.subscribe(params => {
      this._id = params['id'];
      this.interclubsService.getByID(this.id).subscribe(interclubs => {
        interclubs.selections.sort((s1) => (s1.homeOrAway === 'Home') ? -1 : 1);
        interclubs.selections.forEach(s => {
          s.players.sort((p1, p2) => (p1.position < p2.position) ? -1 : 1);
        });
        interclubs.matches.sort((m1, m2) => (m1.position < m2.position) ? -1 : 1);
        interclubs.matches.forEach(m => {
          m.sets.sort((s1, s2) => (s1.position < s2.position) ? -1 : 1);
          if (this._homeScoreEvolution.length === 0) {
            this._homeScoreEvolution.push((m.homeTotal > m.awayTotal) ? 1 : 0);
          } else {
            this._homeScoreEvolution.push(
              this._homeScoreEvolution[this._homeScoreEvolution.length - 1] + ((m.homeTotal > m.awayTotal) ? 1 : 0));
          }
          if (this._awayScoreEvolution.length === 0) {
            this._awayScoreEvolution.push((m.homeTotal < m.awayTotal) ? 1 : 0);
          } else {
            this._awayScoreEvolution.push(
              this._awayScoreEvolution[this._awayScoreEvolution.length - 1] + ((m.homeTotal < m.awayTotal) ? 1 : 0));
          }
          this.homeTotalSets += m.homeTotal;
          this.awayTotalSets += m.awayTotal;
        });
        console.log(interclubs);
        this.interclubs = interclubs;
      }, e => {
        console.log('Erreur vilaine : ' + e.toString());
      });
    });
  }

  edit(event) {
    event.target.className = 'border border-dark text-align-center w-41 h-18 lh-17 editable';
    document.execCommand('selectAll', false, null);
  }

  focusout(m: any, s: any, event) {
    event.target.className = 'border border-dark text-align-center w-41 h-18 lh-17';
    let data = null;
    // Check for existing previous sets ; allowing encoding set by set only starting by the first
    let previousSetsOK = true;
    let set = 0;
    while (previousSetsOK && set < s) {
      if (set > this.interclubs.matches[m].sets.length - 1) {
        previousSetsOK = false;
      }
      set++;
    }
    // Check for set validity
    const matches = this.interclubs.matches;
    const scores = event.target.textContent.split('-');
    const wasUndefined = (m >= matches.length || s >= matches[m].sets.length);
    const valueChanged = (wasUndefined || matches[m].sets[s].homeScore + '-' + matches[m].sets[s].awayScore !== event.target.textContent);
    if (previousSetsOK && Array.isArray(scores) && valueChanged
      && scores.length === 2 && Number.isInteger(parseInt(scores[0], 10)) && Number.isInteger(parseInt(scores[1], 10))) {
      const homeScore = parseInt(scores[0], 10);
      const awayScore = parseInt(scores[1], 10);
      if ((homeScore === 11 && homeScore - awayScore > 1)
        || (awayScore === 11 && awayScore - homeScore > 1)
        || (Math.abs(homeScore - awayScore) === 2) && homeScore > 9 && awayScore > 9) {
        data = {
          homeScore: homeScore,
          awayScore: awayScore,
        };
      }
    }
    // Si changement valide
    if (data !== null) {
      if (this.interclubs.matches[m].sets[s] instanceof Object) {
        this.setService.update(this.interclubs.matches[m].id, s + 1, data).subscribe(() => {
          this.interclubs.matches[m].sets[s].homeScore = scores[0];
          this.interclubs.matches[m].sets[s].awayScore = scores[1];
        });
      } else if (this.interclubs.matches[m] instanceof Object) {
        data['position'] = s + 1;
        this.setService.add(this.interclubs.matches[m].id, data).subscribe(setId => {
          this.interclubs.matches[m].sets.push(new PingSet(setId, s + 1, scores[0], scores[1]));
        });
      } else {
        const matchData = {
          interclubsId: this.id,
          position: m + 1,
        };
        this.matchService.add(matchData).subscribe(matchId => {
          this.interclubs.matches.push(new Match(matchId, this.id, m + 1, [], 0, 0));
          data['position'] = s + 1;
          this.setService.add(matchId, data).subscribe(setId => {
            this.interclubs.matches[m].sets.push(new PingSet(setId, s + 1, scores[0], scores[1]));
          });
        });
      }
    }
  }

  public checkValidPreviousSets(match: Match, previousSetsNb: number, newSet: PingSet = null) {
    let valid = true;
    if (previousSetsNb === 0 || match.sets !== null) {
      let homeSets = 0;
      let awaySets = 0;
      let s = 0;
      while (valid && s < previousSetsNb && s < match.sets.length) {
        homeSets += (match.sets[s].homeScore > match.sets[s].awayScore) ? 1 : 0;
        awaySets += (match.sets[s].homeScore < match.sets[s].awayScore) ? 1 : 0;
        valid = !(awaySets > match.awayTotal || homeSets > match.homeTotal ||
          (awaySets === 3 && homeSets < match.homeTotal) || (homeSets === 3 && awaySets < match.awayTotal));
        s++;
      }
      if (newSet !== null) {
        homeSets += (newSet.homeScore > newSet.awayScore) ? 1 : 0;
        awaySets += (newSet.homeScore < newSet.awayScore) ? 1 : 0;
        valid = !(homeSets === 3 && awaySets < match.awayTotal || awaySets === 3 && homeSets < match.homeTotal);
      }
    } else {
      valid = false;
    }
    return valid;
  }

  editEndTime(event) {
    event.target.className = 'w-42 text-align-center border-bottom border-dark p-0 h-18 editable';
    document.execCommand('selectAll', false, null);
  }

  focusoutEndTime(event) {
    event.target.className = 'w-42 text-align-center border-bottom border-dark p-0 h-18 lh-17';
    let data = null;
    let hours = null;
    let minutes = null;
    // Check for set validity
    const endTime = event.target.textContent.split(':');
    if (Array.isArray(endTime) && endTime.length === 2 && Number.isInteger(endTime[0]) && Number.isInteger(endTime[1])) {
      hours = endTime[0];
      minutes = endTime[1];
      if (0 <= hours && hours < 24 && 0 <= minutes && minutes < 60) {
        data = {
          hours: hours,
          minutes: minutes,
        };
      }
    }

    // Si changement valide
    if (data !== null) {
      this.interclubsService.update(this.id, data).subscribe(updated => {
        this.interclubs.endTime = new Date(0, 0, 0, hours, minutes, 0);
        console.log(updated);
        console.log(event.target.textContent);
        console.log(this.interclubs.endTime);
      });
    }
  }
}
