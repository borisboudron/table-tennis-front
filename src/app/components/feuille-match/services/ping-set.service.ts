import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PingSetService {

  constructor(private client: HttpClient) { }

  update(matchId: number, setPosition: number, setData: any): Observable<Boolean> {
    let httpParams = new HttpParams();
    for (const property of Object.keys(setData)) {
      httpParams = httpParams.append(property, setData[property]);
    }
    return this.client.put<Boolean>('http://tttheux.local/matches/' + matchId + '/sets/' + setPosition, httpParams);
  }

  add(matchId: number, setData: any): Observable<number> {
    let httpParams = new HttpParams();
    for (const property of Object.keys(setData)) {
      httpParams = httpParams.append(property, setData[property]);
    }
    return this.client.post<number>('http://tttheux.local/matches/' + matchId + '/sets', httpParams);
  }
}
