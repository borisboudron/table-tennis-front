import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Interclubs} from '../models/interclubs';

@Injectable({
  providedIn: 'root'
})
export class InterclubsService {

  constructor(private client: HttpClient) {
  }

  getAllByTeam(clubId: string, teamId: number): Observable<Interclubs[]> {
    return this.client.get<Interclubs[]>('http://tttheux.local/clubs/' + clubId + '/' + teamId + '/interclubs');
  }

  getByID(id: number): Observable<Interclubs> {
    return this.client.get<Interclubs>('http://tttheux.local/interclubs/' + id);
  }

  update(id: number, data: any) {
    let httpParams = new HttpParams();
    for (const property of Object.keys(data)) {
      httpParams = httpParams.append(property, data[property]);
    }
    return this.client.patch<Boolean>('http://tttheux.local/interclubs/' + id, httpParams);
  }
}
