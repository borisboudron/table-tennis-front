import { TestBed } from '@angular/core/testing';

import { PingSetService } from './ping-set.service';

describe('PingSetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PingSetService = TestBed.get(PingSetService);
    expect(service).toBeTruthy();
  });
});
