import { TestBed } from '@angular/core/testing';

import { InterclubsService } from './interclubs.service';

describe('InterclubsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterclubsService = TestBed.get(InterclubsService);
    expect(service).toBeTruthy();
  });
});
