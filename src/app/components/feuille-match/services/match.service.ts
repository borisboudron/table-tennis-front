import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor(private client: HttpClient) { }

  add(setData: any): Observable<number> {
    let httpParams = new HttpParams();
    for (const property of Object.keys(setData)) {
      httpParams = httpParams.append(property, setData[property]);
    }
    return this.client.post<number>('http://tttheux.local/matches', httpParams);
  }
}
