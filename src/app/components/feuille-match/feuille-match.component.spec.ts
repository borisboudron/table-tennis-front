import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeuilleMatchComponent } from './feuille-match.component';

describe('FeuilleMatchComponent', () => {
  let component: FeuilleMatchComponent;
  let fixture: ComponentFixture<FeuilleMatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeuilleMatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeuilleMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
