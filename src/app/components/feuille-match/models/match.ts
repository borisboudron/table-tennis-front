import {PingSet} from './ping-set';

export class Match {
  constructor(
    private _id: number,
    private _interclubsId: number,
    private _position: number,
    private _sets: PingSet[],
    private _homeTotal: number,
    private _awayTotal: number,
  ) {}

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get interclubsId(): number {
    return this._interclubsId;
  }

  set interclubsId(value: number) {
    this._interclubsId = value;
  }

  get position(): number {
    return this._position;
  }

  set position(value: number) {
    this._position = value;
  }

  get sets(): PingSet[] {
    return this._sets;
  }

  set sets(value: PingSet[]) {
    this._sets = value;
  }

  get homeTotal(): number {
    return this._homeTotal;
  }

  set homeTotal(value: number) {
    this._homeTotal = value;
  }

  get awayTotal(): number {
    return this._awayTotal;
  }

  set awayTotal(value: number) {
    this._awayTotal = value;
  }
}
