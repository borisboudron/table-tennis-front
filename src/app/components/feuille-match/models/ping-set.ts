export class PingSet {
  constructor (
    private _id: number,
    private _position: number,
    private _homeScore: number,
    private _awayScore: number
  ) {}

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get position(): number {
    return this._position;
  }

  set position(value: number) {
    this._position = value;
  }

  get homeScore(): number {
    return this._homeScore;
  }

  set homeScore(value: number) {
    this._homeScore = value;
  }

  get awayScore(): number {
    return this._awayScore;
  }

  set awayScore(value: number) {
    this._awayScore = value;
  }
}
