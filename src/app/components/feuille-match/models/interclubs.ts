import {Venue} from './venue';
import {League} from './league';
import {Match} from './match';
import {PingSelection} from '../../selections/models/ping-selection';
import {Player} from '../../player/models/player';

export class Interclubs {
  constructor(
    private _interclubsNb: string,
    private _venue: Venue,
    private _league: League,
    private _date: Date,
    private _startTime: Date,
    private _endTime: Date,
    private _selections: PingSelection[],
    private _matches: Match[],
    private _homeCaptain: Player,
    private _awayCaptain: Player,
    private _referee: Player,
  ) {
  }

  get interclubsNb(): string {
    return this._interclubsNb;
  }

  set interclubsNb(value: string) {
    this._interclubsNb = value;
  }

  get venue(): Venue {
    return this._venue;
  }

  set venue(value: Venue) {
    this._venue = value;
  }

  get league(): League {
    return this._league;
  }

  set league(value: League) {
    this._league = value;
  }

  get date(): Date {
    return this._date;
  }

  set date(value: Date) {
    this._date = value;
  }

  get startTime(): Date {
    return this._startTime;
  }

  set startTime(value: Date) {
    this._startTime = value;
  }

  get endTime(): Date {
    return this._endTime;
  }

  set endTime(value: Date) {
    this._endTime = value;
  }

  get selections(): PingSelection[] {
    return this._selections;
  }

  set selections(value: PingSelection[]) {
    this._selections = value;
  }

  get matches(): Match[] {
    return this._matches;
  }

  set matches(value: Match[]) {
    this._matches = value;
  }

  get homeCaptain(): Player {
    return this._homeCaptain;
  }

  set homeCaptain(value: Player) {
    this._homeCaptain = value;
  }

  get awayCaptain(): Player {
    return this._awayCaptain;
  }

  set awayCaptain(value: Player) {
    this._awayCaptain = value;
  }

  get referee(): Player {
    return this._referee;
  }

  set referee(value: Player) {
    this._referee = value;
  }
}
