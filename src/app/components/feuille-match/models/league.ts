import {Category} from './category';

export class League {
  constructor (
    private _id: number,
    private _category: Category,
    private _division: number,
    private _serie: string
  ) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get category(): Category {
    return this._category;
  }

  set category(value: Category) {
    this._category = value;
  }

  get division(): number {
    return this._division;
  }

  set division(value: number) {
    this._division = value;
  }

  get serie(): string {
    return this._serie;
  }

  set serie(value: string) {
    this._serie = value;
  }
}
