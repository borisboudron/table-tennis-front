import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Player} from '../../player/models/player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private client: HttpClient) {
  }

  getAll(): Observable<Player[]> {
    return this.client.get<Player[]>('http://tttheux.local/clubs/L184/players');
  }

  getByID(id: number): Observable<Player> {
    return this.client.get<Player>('http://tttheux.local/players/' + id);
  }

  update(id: number, player: string[]): Observable<boolean> {
    let httpParams = new HttpParams();
    for (const property of Object.keys(player)) {
      httpParams = httpParams.append(property, player[property]);
    }
    return this.client.patch<boolean>('http://tttheux.local/players/' + id, httpParams);
  }
}
