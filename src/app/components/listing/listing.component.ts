import {Component, OnInit} from '@angular/core';
import {Player} from '../player/models/player';
import {PlayerService} from './services/player.service';
import 'datatables.net';
import 'datatables.net-bs4';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {

  private _players: Player[];

  get players(): Player[] {
    return this._players;
  }

  set players(value: Player[]) {
    this._players = value;
  }

  constructor(private playerService: PlayerService) {
  }

  ngOnInit() {
    this.playerService.getAll().subscribe(players => {
      this.players = players;
    });
  }

  focusout(index: number, id: number, param: string, event) {
    event.target.className = '';
    let toUpdate = null;
    // Check for changed params
    // tslint:disable-next-line:triple-equals
    if (this.players[index][param] != event.target.textContent
      && (this.players[index][param] !== null || event.target.textContent !== '')) {
      toUpdate = {};
      toUpdate[param] = event.target.textContent;
    }
    if (toUpdate !== null) {
      this.playerService.update(id, toUpdate).subscribe(updated => {
        console.log(updated);
        // Update changed param
        this.players[index][param] = toUpdate[param];
      });
    }
  }

  edit(event) {
    event.target.className = 'editable';
  }
}
