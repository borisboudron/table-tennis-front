import {Component, Input, OnInit} from '@angular/core';
import {forkJoin} from 'rxjs';
import {map} from 'rxjs/operators';
import {PlayerService} from '../../listing/services/player.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {


  get csv(): string {
    return this._csv;
  }

  set csv(value: string) {
    this._csv = value;
  }

  get fileLoaded(): boolean {
    return this._fileLoaded;
  }

  @Input() set fileLoaded(value: boolean) {
    this._fileLoaded = value;
  }

  get series(): string[][][] {
    return this._series;
  }

  set series(value: string[][][]) {
    this._series = value;
  }

  constructor(private playerService: PlayerService) { }
  private _csv: string;
  private _fileLoaded;
  private _series: string[][][];

  ngOnInit() {
  }

  public changeListener(files: FileList) {
    if (files && files.length > 0) {
      const file: File = files.item(0);
      const reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = () => {
        if (typeof (reader.result) === 'string') {
          this.csv = reader.result;
          this.fileLoaded = true;
          const lines = this.csv.split('\n');

          // filter to remove header and end lines
          forkJoin(lines.filter((line, index) => {
            return index !== 0 && index !== lines.length - 1;
            // map to create an array of Observable<Player>
          }).map(line => this.playerService.getByID(Number.parseInt(line.split(';')[0], 10))
            .pipe(map(player => {
              player['serieId'] = line.split(';')[1];
              return player;
            }))
          )).subscribe(players => {
            const series = [];
            players.forEach(player => {
              if (!(player['serieId'] in series)) {
                series[player['serieId']] = [];
              }
              series[player['serieId']].push({
                id: player.id,
                lastName: player.lastName,
                firstName: player.firstName,
                ranking: player.ranking,
                serieId: player['serieId'],
              });
            });
            Object.keys(series).forEach(key => {
              console.log(series[key]);
              series[key].sort((p1, p2) => {
                if (p1.ranking === p2.ranking) {
                  return (Math.random() < 0.5) ? -1 : 1;
                }
                return (p1.ranking < p2.ranking) ? -1 : 1;
              });
            });
            this.series = series;
          });
        }
      };
    }
  }

}
