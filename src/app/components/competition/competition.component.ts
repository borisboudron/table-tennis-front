import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.css']
})
export class CompetitionComponent implements OnInit {

  private _fileLoaded = false;
  constructor() {
  }

  get fileLoaded(): boolean {
    return this._fileLoaded;
  }

  set fileLoaded(value: boolean) {
    this._fileLoaded = value;
  }


  ngOnInit() {
  }
}
