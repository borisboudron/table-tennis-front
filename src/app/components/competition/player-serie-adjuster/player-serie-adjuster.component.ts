import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-player-serie-adjuster',
  templateUrl: './player-serie-adjuster.component.html',
  styleUrls: ['./player-serie-adjuster.component.css']
})
export class PlayerSerieAdjusterComponent implements OnInit {
  private _headers = [
    'Numéro d\'affiliation',
    'Nom',
    'Prénom',
    'Classement',
    'Série',
  ];
  private _series: string[][][];

  get headers(): string[] {
    return this._headers;
  }

  set headers(value: string[]) {
    this._headers = value;
  }

  get series(): string[][][] {
    return this._series;
  }

  @Input() set series(value: string[][][]) {
    this._series = value;
  }

  constructor() { }

  ngOnInit() {
  }

  divisionChanged(key: any, i: number, event) {
    this.series[event.target.value].push(this.series[key][i]);
    this.series[key].splice(i, 1);
  }

}
