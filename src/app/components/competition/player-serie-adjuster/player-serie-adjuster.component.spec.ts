import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerSerieAdjusterComponent } from './player-serie-adjuster.component';

describe('PlayerSerieAdjusterComponent', () => {
  let component: PlayerSerieAdjusterComponent;
  let fixture: ComponentFixture<PlayerSerieAdjusterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerSerieAdjusterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerSerieAdjusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
