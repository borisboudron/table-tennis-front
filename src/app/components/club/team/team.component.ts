import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  private _matches: any[];
  constructor() { }

  get matches(): any {
    return this._matches;
  }

  @Input() set matches(value: any) {
    this._matches = value;
  }

  ngOnInit() {
  }

}
