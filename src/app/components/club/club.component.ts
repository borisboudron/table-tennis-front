import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TeamService} from '../selections/services/team.service';
import {PlayerService} from '../listing/services/player.service';
import {Player} from '../player/models/player';
import {InterclubsService} from '../feuille-match/services/interclubs.service';

@Component({
  selector: 'app-club',
  templateUrl: './club.component.html',
  styleUrls: ['./club.component.css']
})
export class ClubComponent implements OnInit {
  private _id: string;
  private _clubName: string = null;
  private _teams: any = null;
  private _players: Player[] = null;
  private _team: any = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private interclubsService: InterclubsService,
    private teamService: TeamService,
    private playerService: PlayerService,
  ) {
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get clubName(): string {
    return this._clubName;
  }

  set clubName(value: string) {
    this._clubName = value;
  }

  get teams(): any {
    return this._teams;
  }

  set teams(value: any) {
    this._teams = value;
  }

  get players(): Player[] {
    return this._players;
  }

  set players(value: Player[]) {
    this._players = value;
  }

  get team(): any {
    return this._team;
  }

  set team(value: any) {
    this._team = value;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.teamService.getAllByClub2(this.id).subscribe(club => {
        this.clubName = club.ClubName;
        this.teams = club.TeamEntries;
        console.log(this.teams);
        this.playerService.getAll().subscribe(players => {
          this.players = players;
        });
      });
    });
  }

  toTeam(team: any) {
    this.interclubsService.getAllByTeam(this.id, team.Team).subscribe(matches => {
      this.team = matches;
      console.log(matches);
    });
  }
}
