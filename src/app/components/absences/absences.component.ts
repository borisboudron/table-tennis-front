import {Component, OnInit} from '@angular/core';
import {AbsenceService} from './services/absence.service';

@Component({
  selector: 'app-absences',
  templateUrl: './absences.component.html',
  styleUrls: ['./absences.component.css']
})
export class AbsencesComponent implements OnInit {
  private _weekDay = '1';
  private _days: number[];
  private _absences: any[];
  private _dayAbsences: any[];

  get weekDay(): string {
    return this._weekDay;
  }

  set weekDay(value: string) {
    this._weekDay = value;
  }

  get days(): number[] {
    return this._days;
  }

  set days(value: number[]) {
    this._days = value;
  }

  get absences(): any {
    return this._absences;
  }

  set absences(value: any) {
    this._absences = value;
  }

  get dayAbsences(): any {
    return this._dayAbsences;
  }

  set dayAbsences(value: any) {
    this._dayAbsences = value;
  }

  constructor(private absenceService: AbsenceService) {
  }

  ngOnInit() {
    this.days = [];
    for (let i = 1; i <= 22; i++) {
      this._days.push(i);
    }
    this.absenceService.getAll().subscribe(absences => {
      this.absences = absences;
      this.loadDayAbsences(this.weekDay);
    });
  }

  dayChanged() {
    this.loadDayAbsences(this.weekDay);
  }

  loadDayAbsences(day: string) {
    this.dayAbsences = [];
    for (const absence of this.absences) {
      // tslint:disable-next-line:triple-equals
      if (absence.weekDay == day) {
        this.dayAbsences.push(absence);
      }
    }
  }

  onAdded(absence) {
    this.absences.push(absence);
    this.loadDayAbsences(this.weekDay);
  }

  onDeleted(index) {
    this.dayAbsences.splice(index, 1);
  }
}
