import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Absence} from '../models/absence';

@Injectable({
  providedIn: 'root'
})
export class AbsenceService {
  constructor(private client: HttpClient) {
  }

  getAll(): Observable<Absence[]> {
    return this.client.get<Absence[]>('http://tttheux.local/absences');
  }

  getByID(id: number): Observable<Absence> {
    return this.client.get<Absence>('http://tttheux.local/absences/' + id);
  }

  update(id: number, absence: string[]): Observable<boolean> {
    const httpParams = new HttpParams().set('absence', JSON.stringify(absence));
    return this.client.patch<boolean>('http://tttheux.local/absences/' + id, httpParams);
  }

  add(absence: any[]): Observable<number> {
    const httpParams = new HttpParams().set('absence', JSON.stringify(absence));
    return this.client.post<number>('http://tttheux.local/absences', httpParams);

  }

  delete(id: number) {
    return this.client.delete<boolean>('http://tttheux.local/absences/' + id);
  }
}
