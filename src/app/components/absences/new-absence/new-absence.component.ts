import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PlayerService} from '../../listing/services/player.service';
import {Absence} from '../models/absence';
import {AbsenceService} from '../services/absence.service';

@Component({
  selector: 'app-new-absence',
  templateUrl: './new-absence.component.html',
  styleUrls: ['./new-absence.component.css']
})
export class NewAbsenceComponent implements OnInit {
  private _weekDay: string;
  private _players: any[] = null;
  private _form: FormGroup;
  private _absence: Absence;
  private _added = new EventEmitter<Absence>();

  get weekDay(): string {
    return this._weekDay;
  }

  @Input('wDay') set weekDay(value: string) {
    this._weekDay = value;
  }

  get players(): any[] {
    return this._players;
  }

  set players(value: any[]) {
    this._players = value;
  }

  get form(): FormGroup {
    return this._form;
  }

  set form(value: FormGroup) {
    this._form = value;
  }

  get absence(): Absence {
    return this._absence;
  }

  set absence(value: Absence) {
    this._absence = value;
  }

  @Output() get added(): EventEmitter<Absence> {
    return this._added;
  }

  set added(value: EventEmitter<Absence>) {
    this._added = value;
  }

  constructor(private builder: FormBuilder, private playerService: PlayerService, private absenceService: AbsenceService) {
  }

  ngOnInit() {
    this.formGroupInit();
    this.playerService.getAll().subscribe(players => {
      this.players = players;
    });
  }

  formGroupInit () {
    this._form = this.builder.group({
      'playerId': ['', [
        Validators.required
      ]],
      'weekDay': [this.weekDay, [
        Validators.required
      ]],
      'informedDate': ['', [
        Validators.required
      ]],
      'informedMethod': ['', [
        Validators.required
      ]],
      'reason': ['', [
        Validators.required,
      ]],
    });
  }

  submit() {
    this.form.value.weekDay = this.weekDay;
    if (this.form.valid) {
      this.absenceService.add(this.form.value).subscribe(id => {
        this.absenceService.getByID(id).subscribe(absence => {
          this.added.emit(absence);
          this.formGroupInit();
        });
      });
    }
  }

  delete() {

  }
}
