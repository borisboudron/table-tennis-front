import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Absence} from '../models/absence';
import {AbsenceService} from '../services/absence.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-absence',
  templateUrl: './absence.component.html',
  styleUrls: ['./absence.component.css']
})
export class AbsenceComponent implements OnInit {

  private _index: number;
  private _absence: Absence;
  private _form: FormGroup;
  private _deleted = new EventEmitter<number>();

  get index(): number {
    return this._index;
  }

  @Input() set index(value: number) {
    this._index = value;
  }

  get absence(): Absence {
    return this._absence;
  }

  @Input() set absence(value: Absence) {
    this._absence = value;
  }


  get form(): FormGroup {
    return this._form;
  }

  set form(value: FormGroup) {
    this._form = value;
  }

  @Output() get deleted(): EventEmitter<number> {
    return this._deleted;
  }

  set deleted(value: EventEmitter<number>) {
    this._deleted = value;
  }

  constructor(private absenceService: AbsenceService, private builder: FormBuilder) {
    this._form = this.builder.group({
      'player': ['test', [
        Validators.required
      ]],
      'informedDate': ['test2', [
        Validators.required
      ]],
      'informedMethod': ['test3', [
        Validators.required
      ]],
      'reason': ['test4', [
        Validators.required,
      ]],
    });

  }

  ngOnInit() {
  }

  focusout(param: string, event) {
    event.target.className = '';
    let toUpdate = null;
    // Check for changed params
    // tslint:disable-next-line:triple-equals
    if (this.absence[param] != event.target.textContent
      && (this.absence[param] !== null || event.target.textContent !== '')) {
      toUpdate = {};
      toUpdate[param] = event.target.textContent;
    }
    if (toUpdate !== null) {
      this.update(this.absence.id, param, toUpdate);
    }
  }

  edit(event) {
    event.target.className = 'editable';
  }

  update(id: number, param: string, toUpdate: string[]) {
    this.absenceService.update(id, toUpdate).subscribe(() => {
      // Update changed param
      // Propagation to parent ?
      this.absence[param] = toUpdate[param];
    });
  }

  delete(id: number) {
    this.absenceService.delete(id).subscribe(() => {
      this.deleted.emit(this.index);
    });
  }
}
