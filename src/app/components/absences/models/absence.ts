import {Player} from '../../player/models/player';

export class Absence {

  private _id: number;
  constructor(
    private _playerId: number = null,
    private _player: Player = null,
    private _informedDate: Date,
    private _informedMethod: string,
    private _reason: string
  ) { }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get player(): Player {
    return this._player;
  }

  set player(value: Player) {
    this._player = value;
  }

  get informedDate(): Date {
    return this._informedDate;
  }

  set informedDate(value: Date) {
    this._informedDate = value;
  }

  get informedMethod(): string {
    return this._informedMethod;
  }

  set informedMethod(value: string) {
    this._informedMethod = value;
  }

  get reason(): string {
    return this._reason;
  }

  set reason(value: string) {
    this._reason = value;
  }
}
