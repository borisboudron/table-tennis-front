import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from '../components/home/home.component';
import {PageNotFoundComponent} from '../components/page-not-found/page-not-found.component';
import {ListingComponent} from '../components/listing/listing.component';
import {PlayerComponent} from '../components/player/player.component';
import {AbsencesComponent} from '../components/absences/absences.component';
import {SelectionsComponent} from '../components/selections/selections.component';
import {FeuilleMatchComponent} from '../components/feuille-match/feuille-match.component';
import {CompetitionComponent} from '../components/competition/competition.component';
import {ClubComponent} from '../components/club/club.component';
import {TeamComponent} from '../components/club/team/team.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'listing', component: ListingComponent},
  {path: 'players/:id', component: PlayerComponent},
  {path: 'absences', component: AbsencesComponent},
  {path: 'selections', component: SelectionsComponent},
  {path: 'interclubs/:id', component: FeuilleMatchComponent},
  {path: 'competition', component: CompetitionComponent},
  {path: 'club/:id', component: ClubComponent},
  {path: 'club/:id/teams/:teamid', component: TeamComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
